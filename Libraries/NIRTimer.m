//
//  NIRTimer.m
//  DirectionService
//
//  Created by Nir Soffer on Wed Jun 04 2003.
//  Copyright (c) 2003 Nir Soffer. All rights reserved.
//

#import "NIRTimer.h"
#import <sys/time.h>

@interface NIRTimer (NIRTimerPrivate)
- (long)_currentTime;
@end

@implementation NIRTimer

// reset
- (void)reset
{
    // Reset with zero microsends
    [self resetWithMicroseconds:0];
}

- (void)resetWithSeconds:(double)newSeconds
{
    // Convert to microseconds and reset with microseconds
    long newMicroseconds = lround(newSeconds * 1000000);
    [self resetWithMicroseconds:newMicroseconds];
}

- (void)resetWithMicroseconds:(long)newMicroseconds
{
    // main reset
    if (isRunning) isRunning = NO;
    time = newMicroseconds;
}

// starting and stoping
- (void)start
{
    if (isRunning) return;
    
    started = [self _currentTime];
    isRunning = YES;
}

- (void)stop
{
    if (! isRunning) return;

    time = time + [self _currentTime] - started;
    isRunning = NO;
}

// reporting time
- (long)microseconds
{
    if (isRunning){
        // report the current total, like -stop, without stoping
        return time + [self _currentTime] - started;       
    } else {
        // report the time recorded
        return time;
    }
}

- (double)seconds
{
    return [self microseconds] / 1000000.0;
}

// reporting elapsed time - from last start
- (long)microsecondsSinceStart
{
    if (isRunning){
        // report the time from the last start
        return [self _currentTime] - started;
    } else {
        // report the time recorded
        return time;
    }
}

- (double)secondsSinceStart
{
    return [self microsecondsSinceStart] / 1000000.0;
}

@end

@implementation NIRTimer (NIRTimerPrivate)

- (long)_currentTime
{
    struct timeval tp;
    gettimeofday(&tp, NULL);
    return (tp.tv_sec * 1000000 + tp.tv_usec);
}

@end
