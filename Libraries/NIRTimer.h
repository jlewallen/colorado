//
//  NIRTimer.h
//  DirectionService
//
//  Created by Nir Soffer on Wed Jun 04 2003.
//  Copyright (c) 2003 Nir Soffer. All rights reserved.
//


#import <Foundation/Foundation.h>


@interface NIRTimer : NSObject
{
    long started;
    long time;
    BOOL isRunning;
}

// reset
- (void)reset;
- (void)resetWithSeconds:(double)newSeconds;
- (void)resetWithMicroseconds:(long)newMicroseconds;

// starting and stoping
- (void)start;
- (void)stop;

// reporting total time
- (long)microseconds;
- (double)seconds;

// reporting elapsed time - from last start
- (long)microsecondsSinceStart;
- (double)secondsSinceStart;

@end
