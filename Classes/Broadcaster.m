//
//  AutomaticDiscovery.m
//  Colorado
//
//  Created by Jacob Lewallen on 3/13/11.
//  Copyright 2011 None. All rights reserved.
//

#include <arpa/inet.h>
#include <netinet/in.h>
#include <sys/socket.h>
#include <unistd.h>

#import "Broadcaster.h"

#define MEDIA_CENTER_DISCOVERY_PORT 5556
#define MEDIA_CENTER_DISCOVERY_MESSAGE "GetMediaServerAddress"

@implementation Broadcaster

- (void)_listen {
	dispatch_queue_t queue = dispatch_queue_create("colorado.listen", 0);
	dispatch_async(queue, ^{
		struct sockaddr_in si_me;
		NSInteger yes = 1;
		NSInteger s = 0;

		while (1) {			
			NSLog(@"Receiving...");

			if (s == 0) {
				if ((s = socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP)) == -1) {
					NSLog(@"Error creating socket: %d", errno);
					[NSThread sleepForTimeInterval:0.01];
					s = 0;
					continue;
				}

				if (setsockopt(s, SOL_SOCKET, SO_BROADCAST, &yes, sizeof(yes)) == -1) {
					NSLog(@"Error setting SO_BROADCAST: %d", errno);
					[NSThread sleepForTimeInterval:0.01];
					close(s);
					s = 0;
					continue;
				}

				memset(&si_me, 0, sizeof(struct sockaddr_in));
				si_me.sin_family = AF_INET;
				si_me.sin_port = htons(5556);
				si_me.sin_addr.s_addr = htonl(INADDR_ANY);
				if (bind(s, (struct sockaddr *)&si_me, sizeof(struct sockaddr)) == -1) { 
					NSLog(@"Error binding: %d", errno);
					[NSThread sleepForTimeInterval:0.01];
					close(s);
					s = 0;
					continue;
				}
			}

			fd_set socks;
			struct timeval t;
			FD_ZERO(&socks);
			FD_SET(s, &socks);
			t.tv_sec = 60;
			if (select(s + 1, &socks, NULL, NULL, &t)) {
				struct sockaddr_in si_other;
				socklen_t slen = sizeof(si_other);
				NSInteger received;
				char buffer[256];
				if ((received = recvfrom(s, buffer, sizeof(buffer), 0, (struct sockaddr *)&si_other, &slen)) == -1) {
					NSLog(@"Error receiving: %d", errno);
					[NSThread sleepForTimeInterval:0.01];
					close(s);
					s = 0;
					continue;
				}

				buffer[received] = 0;
				
				// MediaServer:JLEWALLEN-PC.socal.rr.com:192.168.0.180:52199
				NSString *message = [NSString stringWithUTF8String:buffer];
				NSArray *parts = [message componentsSeparatedByString: @":"];
				if ([parts count] == 4) {
					NSLog(@"'%@'", message);
					dispatch_sync(dispatch_get_main_queue(), ^{
						for (id <AutomaticDiscoveryDelegate> delegate in delegates) {
							[delegate discovered:[parts objectAtIndex:1] 
										 address:[NSString stringWithFormat:@"%@:%@", [parts objectAtIndex:2], [parts objectAtIndex:3], nil]];
						}
					});
				}
			}
		}
		
		close(s);
	});
}

- (void)_ping {
	struct sockaddr_in si_other;
	NSInteger yes = 1;
	NSInteger s;
	
	NSLog(@"Ping");
	
	if ((s = socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP)) == -1) {
		NSLog(@"Error creating socket: %d", errno);
		return;
	}
	
	if (setsockopt(s, SOL_SOCKET, SO_BROADCAST, &yes, sizeof(yes)) == -1) {
		NSLog(@"Error setting SO_BROADCAST: %d", errno);
		close(s);
		return;
	}
	
	memset(&si_other, 0, sizeof(si_other));
	si_other.sin_family = AF_INET;
	si_other.sin_port = (in_port_t)htons(MEDIA_CENTER_DISCOVERY_PORT);
	si_other.sin_addr.s_addr = htonl(INADDR_BROADCAST);
	if (sendto(s, MEDIA_CENTER_DISCOVERY_MESSAGE, strlen(MEDIA_CENTER_DISCOVERY_MESSAGE), 0, 
			   (struct sockaddr *)&si_other, sizeof(struct sockaddr_in)) == -1) { 
		NSLog(@"Error sending: %d", errno);
		close(s);
		return;
	}
	
	for (id <AutomaticDiscoveryDelegate> delegate in delegates) {
		[delegate pinged];
	}
	
	close(s);
}

- (void)tick {
	dispatch_queue_t queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_LOW, 0ul);
	dispatch_async(queue, ^{
		[self _ping];
	});
}

- (Broadcaster *)init {
	if (self = [super init]) {
		delegates = [[NSMutableSet alloc] init];
		[self _listen];
		[self _ping];
	}
	return self;
}

- (void)addDelegate:(id <AutomaticDiscoveryDelegate>)delegate {
	[delegates addObject:delegate];
}

- (void)removeDelegate:(id <AutomaticDiscoveryDelegate>)delegate {
	[delegates removeObject:delegate];
}

- (void)dealloc {
	NSLog(@"Stopping Broadcaster");
	[delegates release];
	[super dealloc];
}

@end
