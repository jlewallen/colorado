//
//  MediaServer.h
//  Colorado
//
//  Created by Jacob Lewallen on 3/13/11.
//  Copyright 2011 None. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface MediaServer : NSObject {
	NSString *name;
	NSString *address;
	NSDate *lastSeen;
	BOOL active;
}

@property (nonatomic, retain) NSString *name;
@property (nonatomic, retain) NSString *address;
@property (nonatomic) BOOL active;

- (MediaServer *)initWithName:(NSString *)n address:(NSString *)a;
- (void)touch;
- (NSInteger)lastSeenIn;
- (BOOL)seenIn:(int)seconds;

@end
