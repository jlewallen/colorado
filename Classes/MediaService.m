//
//  MediaService.m
//  Colorado
//
//  Created by Jacob Lewallen on 3/12/11.
//  Copyright 2011 None. All rights reserved.
//

#import "NSString+URLEscaped.h"
#import "NSString+MD5.h"
#import "GDataXMLNode.h"
#import "ColoradoAppDelegate.h"
#import "MediaService.h"
#import "MediaCenter.h"
#import "MediaServer.h"
#import "NIRTimer.h"

#define MediaServiceErrorDomain @"MediaServiceErrorDomain"
#define MediaServiceUserAgent @"ColoradoApp/1.0"

@implementation MediaService

@synthesize error;

+ (MediaService *)sharedInstance {
	static MediaService *sharedInstance;
	@synchronized (self) {
		if (!sharedInstance) {
			sharedInstance = [[MediaService alloc] init];
		}
		return sharedInstance;
	}
	return nil;
}

- (MediaService *)init {
	if (self = [super init]) {
		timer = [[NIRTimer alloc] init];
	}
	return self;
}

- (void)dealloc {
	[timer release];
	[super dealloc];
}

- (NSString *)_serverAddress {
	return [[[[MediaCenter sharedInstance] activeServer] address] copy];
}

- (NSArray *)_query:(NSString *)method maxCacheAge:(double)seconds XPath:(NSString *)path withParams:(NSArray *)params {
	if (error != nil) {
		[error release];
		error = nil;
	}
	
	[timer reset];
	[timer start];
	
	NSString *address = [self _serverAddress];
	if (address == nil) {
		return nil;
	}
	
	NSMutableString *urlBuilder = [NSMutableString stringWithFormat:@"http://%@/MCWS/v1", address];
	if (![method hasPrefix:@"/"]) {
		[urlBuilder appendString:@"/"];
	}
	[urlBuilder appendString:method];
	[urlBuilder appendString:@"?"];
	[urlBuilder appendString:[params componentsJoinedByString:@"&"]];
	NSURL *url = [NSURL URLWithString:urlBuilder];
	[address release];
	
	NSLog(@"URL: %@\n", url);

	if (![[ColoradoAppDelegate app] hasNetworkingConnection]) {
		error = [[NSError alloc] initWithDomain:NSURLErrorDomain code:0 userInfo:nil];
		return nil;
	}

	[UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
	
	NSMutableURLRequest *req = [NSMutableURLRequest requestWithURL:url cachePolicy:NSURLRequestReloadIgnoringCacheData timeoutInterval:5];
	[req setValue:MediaServiceUserAgent forHTTPHeaderField:@"User-Agent"];
	[req setHTTPMethod:@"GET"];
	NSError *httpError = nil;
	NSURLResponse *response = nil;
	
	NSData *responseData = [NSURLConnection sendSynchronousRequest:req returningResponse:&response error:&httpError];
	
	[UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
	if (httpError != nil) {
		NSLog(@"%@\n", httpError);
		error = [httpError retain];
		return nil;
	}		
	
    #if VERBOSE_LOGGING
	NSLog(@"Response: %s\n", [responseData bytes]);
    #endif
	
	NSError *xmlError = NULL;
	GDataXMLDocument *doc = [[[GDataXMLDocument alloc] initWithData:responseData options:0 error:&xmlError] autorelease];
	if (error != nil) {
		NSLog(@"%@\n", xmlError);
		error = [xmlError retain];
		return nil;
	}
	
	NSArray *rows = [doc nodesForXPath:path error:nil];
	
	[timer stop];
	NSLog(@"Done, retrieved %d rows (from %@) in (%6.3f)\n", [rows count], url, [timer seconds]);	
	
	return rows;
}

- (NSDictionary *)_nodeToDictionary:(GDataXMLElement *)node toDictionaryWithXPaths:(NSArray *)paths forKeys:(NSArray *)keys index:(NSNumber *)index defaults:(NSDictionary *)defaults with:(void (^)(NSMutableDictionary *row))block {
	NSMutableDictionary *mutable = [[NSMutableDictionary alloc] init];
	
	for (NSInteger i = 0; i < [keys count]; ++i) {
		NSString *path = [paths objectAtIndex:i];
		NSString *key = [keys objectAtIndex:i];
		NSArray *matches = [node nodesForXPath:path error:nil];
		NSObject *value = @"";
		if ([matches count]) {
			value = [[matches objectAtIndex:0] stringValue];
		}
		else {
			value = [defaults objectForKey:key];
			if (value == nil) {
			#if VERBOSE_LOGGING
				NSLog(@"Missing: %@ = %@\nFrom:\n%@", key, path, node);
			#else
				NSLog(@"Missing: %@ = %@", key, path);
			#endif
			}
		}
		[mutable setValue:value forKey:key];
	}

	[mutable setValue:index forKey:@"index"];
	
	block(mutable);

	NSDictionary *converted = [NSDictionary dictionaryWithDictionary:mutable];
	[mutable release];
	return converted;
}

- (NSArray *)_nodesToDictionaries:(NSArray *)nodes toArrayWithXPaths:(NSArray *)paths forKeys:(NSArray *)keys defaults:(NSDictionary *)defaults with:(void (^)(NSMutableDictionary *row))block {
	[timer reset];
	[timer start];
	
	NSMutableArray *objects = [[[NSMutableArray alloc] init] autorelease];
	for (GDataXMLElement *node in nodes) {
		NSNumber *index = [NSNumber numberWithInt:[nodes indexOfObjectIdenticalTo:node]];
		[objects addObject:[self _nodeToDictionary:node toDictionaryWithXPaths:paths forKeys:keys index:index defaults:defaults with:block]];
	}
	
	[timer stop];
	NSLog(@"NodesToDictionaries in (%6.3f)\n", [timer seconds]);	
	
	return objects;
}

- (NSArray *)_nodesToDictionaries:(NSArray *)nodes toArrayWithXPaths:(NSArray *)paths forKeys:(NSArray *)keys defaults:(NSDictionary *)defaults {
	return [self _nodesToDictionaries:nodes toArrayWithXPaths:paths forKeys:keys defaults:defaults with:^(NSMutableDictionary *m) { }];
}

- (NSArray *)_nodesToDictionaries:(NSArray *)nodes toArrayWithXPaths:(NSArray *)paths forKeys:(NSArray *)keys {
	return [self _nodesToDictionaries:nodes toArrayWithXPaths:paths forKeys:keys defaults:nil];
}

- (NSArray *)_arrayWithFormat:(NSString *)spec forEach:(NSString *)item, ... {
	NSMutableArray *rows = [NSMutableArray arrayWithObjects:nil];
	
	if (item != nil) {
		va_list var_args;
		[rows addObject: [NSString stringWithFormat:spec, item]];
		va_start(var_args, item);
		while (item = va_arg(var_args, NSString*)) {
			[rows addObject: [NSString stringWithFormat:spec, item]];
		}
		va_end(var_args);
	}
	
	return rows;
}

- (NSArray *)_query:(NSString *)method maxCacheAge:(double)seconds XPath:(NSString *)XPath withParameters:(NSString *)firstParam, ... {
	NSMutableArray *params = [[NSMutableArray alloc] init];
	
	if (firstParam) {
		va_list var_args;
		id parameter;
		[params addObject: firstParam];
		va_start(var_args, firstParam);
		while (parameter = va_arg(var_args, id)) {
			[params addObject: parameter];
		}
		va_end(var_args);
	}
	
	NSArray *response = [self _query:method maxCacheAge:seconds XPath:XPath withParams:params];
	[params release];
	return response;
}

/*
 <?xml version="1.0" encoding="UTF-8" standalone="yes" ?>
 <MPL Version="2.0" Title="MCWS - Files - 7440">
 <Item>
 <Field Name="Key">306210</Field>
 <Field Name="Filename">G:\Media\Music\Brand New- Daisy\04 - Brand New - Daisy - Gasoline.mp3</Field>
 <Field Name="Name">Gasoline</Field>
 <Field Name="Artist">Brand New</Field>
 <Field Name="Album">Daisy</Field>
 <Field Name="Genre">Rock</Field>
 <Field Name="Date">39814</Field>
 <Field Name="Bitrate">254</Field>
 <Field Name="Image File">INTERNAL</Field>
 <Field Name="Rating">4</Field>
 <Field Name="Duration">212.2439999999999998</Field>
 <Field Name="Track #">4</Field>
 <Field Name="Disc #">1</Field>
 <Field Name="Media Type">Audio</Field>
 <Field Name="Date Imported">1297875565</Field>
 <Field Name="Last Played">1299880208</Field>
 <Field Name="Number Plays">39</Field>
 <Field Name="File Type">mp3</Field>
 <Field Name="File Size">7025006</Field>
 <Field Name="Date Created">1258051166</Field>
 <Field Name="Date Modified">1273694231</Field>
 <Field Name="Compression">VBR (MPEG-1 Layer 3)</Field>
 <Field Name="Sample Rate">44100</Field>
 <Field Name="Channels">2</Field>
 <Field Name="Bit Depth">16</Field>
 <Field Name="Complete Album">1</Field>
 </Item> 
*/
- (NSArray *)_getPlaylistItems:(NSArray *)nodes playByIndex:(BOOL)p {
	return [self _nodesToDictionaries:nodes 
					toArrayWithXPaths:[NSArray arrayWithObjects:@"Field[@Name='Media Type']", @"Field[@Name='Key']", @"Field[@Name='Artist']", @"Field[@Name='Name']", @"Field[@Name='Album']", @"Field[@Name='Rating']", nil]
							  forKeys:[NSArray arrayWithObjects:@"type", @"key", @"artist", @"name", @"album", @"rating", nil]
							 defaults:[NSDictionary dictionaryWithObjectsAndKeys:@"", @"rating", nil]
								 with:^(NSMutableDictionary *row) {
									 NSString *key = [row objectForKey:@"key"];
									 NSString *imageUrl = [NSString stringWithFormat:@"MCWS/v1/File/GetImage?File=%@", key, nil];
									 [row setValue:imageUrl forKey:@"imageUrl"];
									 [row setValue:(p ? @"1" : @"0") forKey:@"playByIndex"];
								 }];
}

#define kFields @"Media Type,Key,Artist,Name,Album,Rating"

- (NSArray *)findPlayingNow {
	NSArray *nodes = [self _query:@"Playback/Playlist" maxCacheAge:0 XPath:@"/MPL/Item" withParameters:[NSString stringWithFormat:@"Zone=-1"], [NSString stringWithFormat:@"Fields=%@", [kFields URLEscaped]], nil];
	return [self _getPlaylistItems:nodes playByIndex:YES];
}

- (NSArray *)findPlaylistSongs:(NSString*)playlistId {
	NSArray *nodes = [self _query:@"Playlist/Files" maxCacheAge:0 XPath:@"/MPL/Item" withParameters:[NSString stringWithFormat:@"Playlist=%@", playlistId], @"PlaylistType=ID", @"Action=mpl", [NSString stringWithFormat:@"Fields=%@", [kFields URLEscaped]], nil];
	return [self _getPlaylistItems:nodes playByIndex:NO];
}

/*
 <?xml version="1.0" encoding="UTF-8" standalone="yes" ?>
 <Response Status="OK">
 <Item Name="NumberZones">1</Item>
 <Item Name="CurrentZoneID">0</Item>
 <Item Name="CurrentZoneIndex">0</Item>
 <Item Name="ZoneName0">Jlewallen-Pc</Item>
 <Item Name="ZoneID0">0</Item>
 <Item Name="ZoneGUID0">{D1AD931D-B2F5-4FC3-AB6A-1B9E4EDA28D3}</Item>
 </Response>
*/
- (NSString *)_getString:(GDataXMLNode *)node XPath:(NSString *)path {
	return [[[node nodesForXPath:path error:nil] objectAtIndex:0] stringValue];
}

- (NSArray *)findZones {
	GDataXMLNode *response = [[self _query:@"Playback/Zones" maxCacheAge:0 XPath:@"/Response" withParameters:nil] objectAtIndex:0];
	NSMutableArray *zones = [[NSMutableArray alloc] init];
	int number = [[self _getString:response XPath:@"Item[@Name='NumberZones']/text()"] intValue];
	int currentZone = [[self _getString:response XPath:@"Item[@Name='CurrentZoneIndex']/text()"] intValue];
	for (int i = 0; i < number; ++i) {
		NSMutableDictionary *zone = [NSMutableDictionary dictionaryWithCapacity:3];
		[zone setValue:[self _getString:response XPath:[NSString stringWithFormat:@"Item[@Name='ZoneName%d']/text()", i]] forKey:@"name"];
		[zone setValue:[self _getString:response XPath:[NSString stringWithFormat:@"Item[@Name='ZoneID%d']/text()", i]] forKey:@"id"];
		[zone setValue:[self _getString:response XPath:[NSString stringWithFormat:@"Item[@Name='ZoneGUID%d']/text()", i]] forKey:@"guid"];
		if (i == currentZone) {
			[zone setValue:@"1" forKey:@"active"];
		}
		[zones addObject:zone];
	}

	NSArray *returned = [NSArray arrayWithArray:zones];
	[zones release];
	return returned;
}

/* 
 <?xml version="1.0" encoding="UTF-8" standalone="yes" ?>
 <Response Status="OK">
 <Item Name="State">1</Item>
 <Item Name="FileKey">370837</Item>
 <Item Name="PositionMS">205916</Item>
 <Item Name="DurationMS">302158</Item>
 <Item Name="ElapsedTimeDisplay">3:25</Item>
 <Item Name="RemainingTimeDisplay">-1:37</Item>
 <Item Name="TotalTimeDisplay">5:02</Item>
 <Item Name="PositionDisplay">3:25 / 5:02</Item>
 <Item Name="PlayingNowPosition">44</Item>
 <Item Name="PlayingNowTracks">67</Item>
 <Item Name="PlayingNowPositionDisplay">45 of 67</Item>
 <Item Name="Bitrate">256</Item>
 <Item Name="SampleRate">44100</Item>
 <Item Name="Channels">2</Item>
 <Item Name="Chapter">0</Item>
 <Item Name="Volume">0.57576</Item>
 <Item Name="VolumeDisplay">58%</Item>
 <Item Name="ImageURL">MCWS/v1/File/GetImage?File=370837</Item>
 <Item Name="Artist">Wolfmother</Item>
 <Item Name="Album">Wolfmother</Item>
 <Item Name="Name">Colossal</Item>
 <Item Name="Rating">4</Item>
 <Item Name="Status">Paused</Item>
 </Response>
 */
- (NSDictionary *)findCurrentlyPlaying {
	NSArray *nodes = [self _query:@"Playback/Info" maxCacheAge:0 XPath:@"/Response" withParameters:nil];
	if ([nodes count]) {
		return [[self _nodesToDictionaries:nodes 
						 toArrayWithXPaths:[self _arrayWithFormat:@"Item[@Name='%@']" forEach:@"Status", @"FileKey", @"PositionMS", @"DurationMS", @"PlayingNowPosition", @"Volume", @"Artist", @"Album", @"Name", @"ImageURL", nil]
								   forKeys:[NSArray arrayWithObjects:@"status", @"key", @"position", @"duration", @"playlistPosition", @"volume", @"artist", @"album", @"name", @"imageUrl", nil]] objectAtIndex:0];
		
	}
	return nil;
}

/*
 /Playback/Volume
 
 <?xml version="1.0" encoding="UTF-8" standalone="yes" ?>
 <Response Status="OK">
 <Item Name="Level">0.57576</Item>
 <Item Name="Display">58%</Item>
 </Response>
*/

/*
 /Playback/Position
 
 <?xml version="1.0" encoding="UTF-8" standalone="yes" ?>
 <Response Status="OK">
 <Item Name="Position">205916</Item>
 </Response>
 */

/*
 /Playback/PlayPause?Zone=-1&ZoneType=ID
 
 <?xml version="1.0" encoding="UTF-8" standalone="yes" ?>
 <Response Status="OK"/>
*/
- (void)playPause {
	[self _query:@"Playback/PlayPause" maxCacheAge:0 XPath:@"/" withParameters:nil];
}

/*
 /Playlists/List
 
 <?xml version="1.0" encoding="UTF-8" standalone="yes" ?>
 <Response Status="OK">
 <Item>
 <Field Name="ID">1361956</Field>
 <Field Name="Name">Imported Playlists</Field>
 <Field Name="Path">Imported Playlists</Field>
 <Field Name="Type">Group</Field>
 </Item>
 <Item>
 <Field Name="ID">207579013</Field>
 <Field Name="Name">[A-- B] Life</Field>
 <Field Name="Path">Imported Playlists\[A-- B] Life</Field>
 <Field Name="Type">Playlist</Field>
 </Item>
 <Item>
 <Field Name="ID">721922566</Field>
 <Field Name="Name">Punk Goes Crunk</Field>
 <Field Name="Path">Imported Playlists\Punk Goes Crunk</Field>
 <Field Name="Type">Playlist</Field>
 </Item>
 <Item>
 <Field Name="ID">780264372</Field>
 <Field Name="Name">Smartlists</Field>
 <Field Name="Path">Smartlists</Field>
 <Field Name="Type">Group</Field>
 </Item>
 <Item>
 <Field Name="ID">68846000</Field>
 <Field Name="Name">4+ Stars</Field>
 <Field Name="Path">Smartlists\4+ Stars</Field>
 <Field Name="Type">Smartlist</Field>
 </Item>
 </Response> 
*/
- (NSArray *)findPlaylists {
	NSArray *nodes = [self _query:@"Playlists/List" maxCacheAge:0 XPath:@"/Response/Item" withParameters:nil];
	return [self _nodesToDictionaries:nodes 
					toArrayWithXPaths:[self _arrayWithFormat:@"Field[@Name='%@']" forEach:@"ID", @"Name", @"Path", @"Type", nil]
							  forKeys:[NSArray arrayWithObjects:@"id", @"name", @"path", @"type", nil]];
}

- (NSURL *)relativeUrlOf:(NSString *)relativePath {
	if (relativePath == nil || [relativePath length] == 0) return nil;
	NSString *address = [self _serverAddress];
	if (address == nil) return nil;
	NSMutableString *urlBuilder = [NSMutableString stringWithFormat:@"http://%@", address];
	if (![relativePath hasPrefix:@"/"]) {
		[urlBuilder appendString:@"/"];
	}
	[urlBuilder appendString:relativePath];
	[address release];
	return [NSURL URLWithString:urlBuilder];
}

- (NSArray *)findArtists {
	NSArray *nodes = [self _query:@"Library/Values" maxCacheAge:0 XPath:@"/Response/Item" withParameters:[NSString stringWithFormat:@"Field=Artist"], nil];
	return [self _nodesToDictionaries:nodes 
					toArrayWithXPaths:[NSArray arrayWithObjects:@"text()", nil]
							  forKeys:[NSArray arrayWithObjects:@"name", nil]];
}

- (void)playByIndex:(int)index {
	[self _query:@"Playback/PlayByIndex" maxCacheAge:0 XPath:@"/" withParameters:[NSString stringWithFormat:@"Index=%d", index], nil];
}

- (void)appendByKey:(NSString *)key {
	[self _query:@"Playback/PlayByKey" maxCacheAge:0 XPath:@"/" withParameters:[NSString stringWithFormat:@"Key=%@", key], @"Location=End", nil];
}

- (void)playByKeyNow:(NSString *)key {
	[self _query:@"Playback/PlayByKey" maxCacheAge:0 XPath:@"/" withParameters:[NSString stringWithFormat:@"Key=%@", key], nil];
}

- (void)playByKeyNext:(NSString *)key {
	NSDictionary *currently = [self findCurrentlyPlaying];
	NSInteger position = [[currently objectForKey:@"playlistPosition"] intValue] + 1;
	[self _query:@"Playback/PlayByKey" maxCacheAge:0 XPath:@"/" withParameters:[NSString stringWithFormat:@"Key=%@", key], [NSString stringWithFormat:@"Location=%d", position], nil];
}

- (void)gotoNext {
	[self _query:@"Playback/Next" maxCacheAge:0 XPath:@"/" withParameters:nil];
}

- (void)gotoPrevious {
	[self _query:@"Playback/Previous" maxCacheAge:0 XPath:@"/" withParameters:nil];
}

- (void)volume:(float)level {
	[self _query:@"Playback/Volume" maxCacheAge:0 XPath:@"/" withParameters:[NSString stringWithFormat:@"Level=%f", level], nil];
}

- (NSArray *)search:(NSString *)term {
	NSArray *nodes = [self _query:@"Files/Search" maxCacheAge:0 XPath:@"/MPL/Item[contains(Field[@Name='Media Type']/text(), 'Audio')]" withParameters:[NSString stringWithFormat:@"Query=%@", [term URLEscaped]], @"Action=MPL", nil];
	return [self _getPlaylistItems:nodes playByIndex:NO];
}

- (void)changeZone:(NSString *)id {
	[self _query:@"Playback/SetZone" maxCacheAge:0 XPath:@"/" withParameters:[NSString stringWithFormat:@"Zone=%@", id], nil];
}

@end
