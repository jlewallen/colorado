//
//  WaitingForServersController.m
//  Colorado
//
//  Created by Jacob Lewallen on 3/14/11.
//  Copyright 2011 None. All rights reserved.
//

#import "WaitingForServersController.h"


@implementation WaitingForServersController

@synthesize activityView;

- (void)viewDidLoad {
    [super viewDidLoad];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
	[activityView startAnimating];
}	

- (void)dealloc {
	[activityView release];
    [super dealloc];
}

@end
