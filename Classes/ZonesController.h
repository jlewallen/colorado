//
//  ZonesController.h
//  Colorado
//
//  Created by Jacob Lewallen on 3/13/11.
//  Copyright 2011 None. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface ZonesController : UITableViewController {
	NSArray *zones;
	NSIndexPath *activePath;
}

@end
