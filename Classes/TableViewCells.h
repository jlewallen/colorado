//
//  SongCell.h
//  Colorado
//
//  Created by Jacob Lewallen on 3/12/11.
//  Copyright 2011 None. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface MultiLineTableViewCell :UITableViewCell {
	UILabel *topLabel;
	UILabel *bottomLabel;
}
@property (nonatomic, retain) UILabel *topLabel;
@property (nonatomic, retain) UILabel *bottomLabel;

@end

@interface SongCell : MultiLineTableViewCell {
}

- (void)assign:(NSDictionary *)song;

@end

@class MediaServer;

@interface ServerCell : MultiLineTableViewCell {
}

- (void)assign:(MediaServer *)server;

@end
