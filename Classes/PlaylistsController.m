//
//  PlaylistsController.m
//  Colorado
//
//  Created by Jacob Lewallen on 3/12/11.
//  Copyright 2011 None. All rights reserved.
//

#import "PlaylistsController.h"
#import "MediaService.h"
#import "SongListControllers.h"
#import "ProgressViewController.h"

@interface Grouping : NSObject {
	NSString *title;
	NSPredicate *predicate;
	NSArray *original;
	NSArray *filtered;
}

@property (nonatomic, retain) NSString *title;
@property (nonatomic, retain) NSPredicate *predicate;

- (Grouping *)initWithTitle:(NSString *)t expression:(NSString *)e;
- (NSArray *)apply:(NSArray *)array;

@end

@implementation Grouping

@synthesize title;
@synthesize predicate;

- (Grouping *)initWithTitle:(NSString *)t expression:(NSString *)e {
	if (self = [super init]) {
		self.predicate = [NSPredicate predicateWithFormat:e];
		self.title = t;
		filtered = nil;
	}
	return self;
}

- (NSArray *)apply:(NSArray *)array {
	if (filtered != nil) {
		return filtered;
	}
	filtered = [[NSArray alloc] initWithArray:[array filteredArrayUsingPredicate:predicate]];
	return filtered;
}

- (void)dealloc {
	[filtered release];
	[predicate release];
	[super dealloc];
}

@end

@implementation PlaylistsController

- (void)viewDidLoad {
    [super viewDidLoad];
	[self setTitle:@"Playlists"];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];

	if (playlists == nil) {
		playlists = [[NSArray alloc] init];
	}
	
	[[ProgressViewController sharedInstance] show];
	
	dispatch_queue_t queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0ul);
	dispatch_async(queue, ^{
		NSArray *items = [[MediaService sharedInstance] findPlaylists];
		[groupings release];
		Grouping *g1 = [[Grouping alloc] initWithTitle:@"Playlists" expression:@"(NOT (SELF.path MATCHES '^Recent.*')) AND SELF.type != 'Group'"];
		Grouping *g2 = [[Grouping alloc] initWithTitle:@"Recent Playlists" expression:@"SELF.path MATCHES '^Recent.*' AND SELF.type != 'Group'"];
		groupings = [[NSArray alloc] initWithObjects:g1, g2, nil];
		[g1 release];
		[g2 release];
		[playlists release];
		playlists = [items retain];
        dispatch_sync(dispatch_get_main_queue(), ^{
			[self.tableView reloadData];
			[[ProgressViewController sharedInstance] hide];
        });
    });	
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
}

- (void)viewDidDisappear:(BOOL)animated {
    [super viewDidDisappear:animated];
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
	return [groupings count];
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {
	return [[groupings objectAtIndex:section] title];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
	return [[[groupings objectAtIndex:section] apply:playlists] count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *CellIdentifier = @"PlaylistCell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier] autorelease];
    }
	
	NSArray *group = [[groupings objectAtIndex:indexPath.section] apply:playlists];
	NSDictionary *item = [group objectAtIndex:indexPath.row];
	cell.textLabel.text = [item objectForKey:@"name"];
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
	NSArray *group = [[groupings objectAtIndex:indexPath.section] apply:playlists];
	NSDictionary *item = [group objectAtIndex:indexPath.row];
	PlaylistController *controller = [[PlaylistController alloc] initWithNibName:@"SongListController" bundle:[NSBundle mainBundle]];
	controller.playlistId = [item objectForKey:@"id"];
	controller.title = [item objectForKey:@"name"];
	[self.navigationController pushViewController:controller animated:YES];
}

- (void)viewDidUnload {
}

- (void)dealloc {
	[groupings release];
	[playlists release];
    [super dealloc];
}

@end

