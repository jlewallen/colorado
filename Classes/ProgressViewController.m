//
//  ProgressViewController.m
//  WeatherDoll
//
//  Created by Jacob Lewallen on 3/18/11.
//  Copyright 2011 None. All rights reserved.
//

#import "ProgressViewController.h"
#import "FrontController.h"

@implementation ProgressViewController

@synthesize activityView;

+ (ProgressViewController *)sharedInstance {
	static ProgressViewController *sharedInstance;
	@synchronized (self) {
		if (!sharedInstance) {
			sharedInstance = [[ProgressViewController alloc] initWithNibName:@"ProgressViewController" bundle:[NSBundle mainBundle]];
		}
		return sharedInstance;
	}
	return nil;
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
	[activityView startAnimating];
}

- (void)show {
	[self showIn:[[FrontController sharedInstance] navigationController].view];
}

- (void)showIn:(UIView *)view {
	self.view.frame = view.frame;
	[view addSubview:self.view];
	[activityView startAnimating];
}

- (void)hide {
	[self.view removeFromSuperview];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (void)viewDidUnload {
    [super viewDidUnload];
}

- (void)dealloc {
	[activityView release];
    [super dealloc];
}


@end

