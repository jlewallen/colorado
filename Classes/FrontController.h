//
//  FrontController.h
//  Colorado
//
//  Created by Jacob ; on 3/12/11.
//  Copyright 2011 None. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "MediaCenter.h"

@class CurrentPlaylistController;
@class PlaylistsController;
@class PlayingNowController;
@class SearchController;
@class ZonesController;
@class ServersController;
@class WaitingForServersController;
@class MenuController;
@class ArtistListController;

@interface FrontController : NSObject <MediaCenterDelegate> {
	UIWindow *window;
	UINavigationController *navigationController;
	CurrentPlaylistController *currentPlaylistController;
	PlaylistsController *playlistsController;
	PlayingNowController *playingNowController;
	SearchController *searchController;
	ZonesController *zonesController;
	ServersController *serversController;
	WaitingForServersController *waitingForServersController;
	MenuController *menuController;
	ArtistListController *artistListController;
	MediaCenter *mediaCenter;
}

@property (retain, nonatomic) UIWindow *window;
@property (retain, nonatomic) MenuController *menuController;
@property (retain, nonatomic) CurrentPlaylistController *currentPlaylistController;
@property (retain, nonatomic) PlaylistsController *playlistsController;
@property (retain, nonatomic) PlayingNowController *playingNowController;
@property (retain, nonatomic) UINavigationController *navigationController;
@property (retain, nonatomic) SearchController *searchController;
@property (retain, nonatomic) ZonesController *zonesController;
@property (retain, nonatomic) ServersController *serversController;
@property (retain, nonatomic) WaitingForServersController *waitingForServersController;
@property (retain, nonatomic) ArtistListController *artistListController;
@property (retain, nonatomic) MediaCenter *mediaCenter;

+ (FrontController *)sharedInstance;

- (FrontController *)initWithWindow:(UIWindow *)w;
- (void)showMenu;
- (void)showCurrentPlaylist;
- (void)showPlaylists;
- (void)showPlayingNow:(NSDictionary *)song;
- (void)showSearch;
- (void)showSearchFor:(NSString *)searchString;
- (void)showArtistSearchFor:(NSString *)searchString;
- (void)showZones;
- (void)showServers;
- (void)showWaitingForServers;
- (void)showArtistListController;
- (void)popAll;

@end
