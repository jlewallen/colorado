//
//  AutomaticDiscovery.h
//  Colorado
//
//  Created by Jacob Lewallen on 3/13/11.
//  Copyright 2011 None. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol AutomaticDiscoveryDelegate <NSObject>
@optional
- (void)pinged;
- (void)discovered:(NSString *)n address:(NSString *)a;
@end

@interface Broadcaster : NSObject {
	NSMutableSet *delegates;
}

- (void)addDelegate:(id <AutomaticDiscoveryDelegate>)delegate;
- (void)removeDelegate:(id <AutomaticDiscoveryDelegate>)delegate;
- (void)tick;

@end
