//
//  ColoradoAppDelegate.h
//  Colorado
//
//  Created by Jacob Lewallen on 3/12/11.
//  Copyright 2011 None. All rights reserved.
//

#import <UIKit/UIKit.h>

@class ColoradoViewController;
@class FrontController;

@interface ColoradoAppDelegate : NSObject <UIApplicationDelegate> {
	UIWindow *window;
	UINavigationController *navigationController;
	FrontController *frontController;
}

@property (nonatomic, retain) IBOutlet UIWindow *window;
@property (nonatomic, retain) IBOutlet UINavigationController *navigationController;
@property (nonatomic, retain) IBOutlet FrontController *frontController;

+ (ColoradoAppDelegate *)app;
- (BOOL)hasNetworkingConnection;
- (BOOL)hasWiFiConnection;

@end

