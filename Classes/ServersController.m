//
//  ServersController.m
//  Colorado
//
//  Created by Jacob Lewallen on 3/13/11.
//  Copyright 2011 None. All rights reserved.
//

#import "ServersController.h"
#import "TableViewCells.h"
#import "MediaCenter.h"
#import "MediaServer.h"

@implementation ServersController

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
	[self setTitle:@"Servers"];
	servers = [[[[MediaCenter sharedInstance] servers] allValues] copy];
	[self.tableView reloadData];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [servers count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *CellIdentifier = @"ServerCell";
    
    ServerCell *cell = (ServerCell *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[[ServerCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier] autorelease];
    }
	
	MediaServer *server = [servers objectAtIndex:indexPath.row];
	[cell assign:server];
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
	MediaServer *server = [servers objectAtIndex:indexPath.row];
	[[MediaCenter sharedInstance] use:server];
}

- (void)dealloc {
	[servers release];
    [super dealloc];
}

@end

