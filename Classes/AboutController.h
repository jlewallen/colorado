//
//  AboutController.h
//  Colorado
//
//  Created by Jacob Lewallen on 3/13/11.
//  Copyright 2011 None. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AboutController : UIViewController {
	IBOutlet NSString *runtimeId;
	IBOutlet NSString *libraryVersion;
	IBOutlet NSString *programVersion;
}

@property (nonatomic, retain) IBOutlet NSString *runtimeId;
@property (nonatomic, retain) IBOutlet NSString *libraryVersion;
@property (nonatomic, retain) IBOutlet NSString *programVersion;

@end
