//
//  MediaCenter.m
//  Colorado
//
//  Created by Jacob Lewallen on 3/13/11.
//  Copyright 2011 None. All rights reserved.
//

#import "MediaCenter.h"
#import "MediaService.h"
#import "MediaServer.h"
#import "Broadcaster.h"
#import "FrontController.h"

@implementation MediaCenter

@synthesize servers;
@synthesize activeServer;
@synthesize lastPinged;

+ (MediaCenter *)sharedInstance {
	return [[FrontController sharedInstance] mediaCenter];	
}

- (BOOL)_shouldRefreshCurrentlyPlaying {
	@synchronized (self) {
		for (id <MediaCenterDelegate> delegate in [[delegates copy] autorelease]) {
			if ([delegate respondsToSelector:@selector(mediaCenter:currentPlaying:)]) {
				return YES;
			}
		}
		return NO;
	}
}

- (BOOL)_shouldPurgeServers {
	@synchronized (self) {
		if (self.lastPinged == nil) {
			NSLog(@"No purge.");
			return NO;
		}
		NSTimeInterval timeSincePing = -[self.lastPinged timeIntervalSinceNow];
		NSLog(@"Last Ping: %f", timeSincePing);
		return timeSincePing < 10 && timeSincePing > 1;
	}
}

- (void)_fireServersChanges {
	@synchronized (self) {
		for (id <MediaCenterDelegate> delegate in [[delegates copy] autorelease]) {
			if ([delegate respondsToSelector:@selector(serversChanged:activeServer:)]) {
				[delegate serversChanged:[servers allValues] activeServer:activeServer];
			}
		}
	}
}

- (void)_tick {
	if ([self _shouldRefreshCurrentlyPlaying]) {
		[self findCurrentlyPlaying];		
	}
	
	NSLog(@"Tick...");
	[broadcaster tick];
	
	NSLog(@"Check...");
	if ([self _shouldPurgeServers]) {
		BOOL changed = NO;
		for (MediaServer *server in [servers allValues]) {
			if (![server seenIn:30]) {
				changed = YES;
				NSLog(@"Removing %@ after %d", server.name, [server lastSeenIn]);
				[servers removeObjectForKey:server.name];
				if (activeServer == server) {
					[self use:nil];
				}
			}
		}
		if (changed) {
			[self _fireServersChanges];
		}
	}
}

- (MediaCenter *)initWithDelegate:(id <MediaCenterDelegate>)delegate {
	if (self = [super init]) {
		delegates = [[NSMutableSet alloc] init];
		servers = [[NSMutableDictionary alloc] init];
		broadcaster = [[Broadcaster alloc] init];
		[broadcaster addDelegate:self];
		[self addDelegate:delegate];
		timer = [NSTimer scheduledTimerWithTimeInterval:5 target:self selector:@selector(_tick) userInfo:nil repeats:YES];
	}
	return self;
}

- (void)pinged {
	@synchronized (self) {
		self.lastPinged = [NSDate date];
	}
}

- (void)discovered:(NSString *)n address:(NSString *)a {
	MediaServer *server = [servers objectForKey:n];
	if (server == nil) {
		server = [[MediaServer alloc] initWithName:n address:a];
		[servers setObject:server forKey:server.name];
		if (activeServer == nil) {
			[self use:server];
			[self _fireServersChanges];
		}
	}
	[server touch];
}

- (NSDictionary *)findCurrentlyPlaying {
	NSDictionary *info = [[MediaService sharedInstance] findCurrentlyPlaying];
	if ([[info objectForKey:@"status"] isEqualToString:@"Waiting"]) {
	}
	@synchronized (self) {
		for (id <MediaCenterDelegate> delegate in [[delegates copy] autorelease]) {
			if ([delegate respondsToSelector:@selector(mediaCenter:currentPlaying:)]) {
				[delegate mediaCenter:self currentPlaying:info];
			}
		}
	}
	return info;
}

- (void)addDelegate:(id <MediaCenterDelegate>)delegate {
	@synchronized (self) {
		NSLog(@"Add: %@", delegate);
		[delegates addObject:delegate];
	}
}

- (void)removeDelegate:(id <MediaCenterDelegate>)delegate {
	@synchronized (self) {
		NSLog(@"Remove: %@", delegate);
		[delegates removeObject:delegate];
	}
}

- (void)use:(MediaServer *)server {
	[activeServer setActive:NO];
	[server setActive:YES];
	if (activeServer != server) {
		NSLog(@"Active Server Changed: %@", server);
		@synchronized (self) {
			for (id <MediaCenterDelegate> delegate in [[delegates copy] autorelease]) {
				if ([delegate respondsToSelector:@selector(activeServerChanged:)]) {
					[delegate activeServerChanged:server];
				}
			}		
		}
	}
	activeServer = server;
}

- (void)dealloc {
	[timer invalidate];
	[delegates release];
	[servers release];
	[super dealloc];
}

@end
