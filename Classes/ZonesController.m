//
//  ZonesController.m
//  Colorado
//
//  Created by Jacob Lewallen on 3/13/11.
//  Copyright 2011 None. All rights reserved.
//

#import "ZonesController.h"
#import "MediaService.h"
#import "ProgressViewController.h"
#import "FrontController.h"

@implementation ZonesController

- (void)_refresh {
	if (zones == nil) {
		zones = [[NSArray alloc] init];
	}
	
	[[ProgressViewController sharedInstance] showIn:self.view];

	dispatch_queue_t queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0ul);
	dispatch_async(queue, ^{
		NSArray *items = [[MediaService sharedInstance] findZones];
		[zones release];
		zones = [items retain];
        dispatch_sync(dispatch_get_main_queue(), ^{
			[self.tableView reloadData];
			[[ProgressViewController sharedInstance] hide];
        });
    });	
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
	[self setTitle:@"Zones"];
	[self _refresh];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [zones count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *CellIdentifier = @"ZoneCell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier] autorelease];
    }
	
	NSDictionary *item = [zones objectAtIndex:indexPath.row];
	if ([item objectForKey:@"active"] != nil) {
		cell.accessoryType = UITableViewCellAccessoryCheckmark;
		activePath = indexPath;
	}
	else {
		cell.accessoryType = UITableViewCellAccessoryNone;
	}
	cell.textLabel.text = [item objectForKey:@"name"];
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
	NSDictionary *zone = [zones objectAtIndex:indexPath.row];
	[[MediaService sharedInstance] changeZone:[zone objectForKey:@"id"]];
	[tableView cellForRowAtIndexPath:activePath].accessoryType = UITableViewCellAccessoryNone;
	[tableView cellForRowAtIndexPath:indexPath].accessoryType = UITableViewCellAccessoryCheckmark;
	activePath = indexPath;
	[[FrontController sharedInstance] popAll];
	[[FrontController sharedInstance] showPlayingNow:nil];
}

- (void)dealloc {
	[activePath release];
	[zones release];
    [super dealloc];
}

@end

