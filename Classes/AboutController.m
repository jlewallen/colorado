//
//  AboutController.m
//  Colorado
//
//  Created by Jacob Lewallen on 3/13/11.
//  Copyright 2011 None. All rights reserved.
//

#import "AboutController.h"


@implementation AboutController

@synthesize runtimeId;
@synthesize libraryVersion;
@synthesize programVersion;

- (void)dealloc {
    [super dealloc];
}

@end
