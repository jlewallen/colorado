//
//  ProgressViewController.h
//  WeatherDoll
//
//  Created by Jacob Lewallen on 3/18/11.
//  Copyright 2011 None. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ProgressViewController : UIViewController {
	IBOutlet UIActivityIndicatorView *activityView;
}

@property (nonatomic, retain) IBOutlet UIActivityIndicatorView *activityView;

+ (ProgressViewController *)sharedInstance;

- (void)viewWillAppear:(BOOL)animated;
- (void)show;
- (void)showIn:(UIView *)view;
- (void)hide;

@end
