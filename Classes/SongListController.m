//
//  SongListController.m
//  Colorado
//
//  Created by Jacob Lewallen on 3/12/11.
//  Copyright 2011 None. All rights reserved.
//

#import "SongListController.h"
#import "ProgressViewController.h"
#import "FrontController.h"
#import "MediaService.h"
#import "TableViewCells.h"
#import "NIRTimer.h"

@implementation SongListController

@synthesize filter;

- (void)needSongs {
}

- (void)viewDidLoad {
    [super viewDidLoad];	
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];

	if (visibleSongs == nil) {
		visibleSongs = [[NSArray array] retain];
	}

	[self needSongs];	
}

- (void)showSongs:(NSArray *)s predicate:(NSPredicate *)predicate {
	dispatch_sync(dispatch_get_main_queue(), ^{
		[allSongs release];
		[visibleSongs release];
		allSongs = nil;
		visibleSongs = nil;
		allSongs = [s retain];
		
		if (predicate != nil) {
			visibleSongs = [[allSongs filteredArrayUsingPredicate:predicate] retain];
		}
		else {
			visibleSongs = [allSongs retain];
		}
		
		selectedSong = nil;
		[self.tableView reloadData];
    	[[ProgressViewController sharedInstance] hide];
	});
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
}

- (void)viewDidDisappear:(BOOL)animated {
    [super viewDidDisappear:animated];
}

- (void)searchDisplayControllerDidEndSearch:(UISearchDisplayController *)controller {
	[visibleSongs release];
	visibleSongs = [allSongs retain];
}

- (BOOL)searchDisplayController:(UISearchDisplayController *)controller shouldReloadTableForSearchScope:(NSInteger)searchOption {
	return YES;
}

- (BOOL)searchDisplayController:(UISearchDisplayController *)controller shouldReloadTableForSearchString:(NSString *)searchString {
	NSPredicate *predicate = [NSPredicate predicateWithFormat:@"SELF.name contains[cd] %@", searchString];
	[visibleSongs release];
	visibleSongs = [[allSongs filteredArrayUsingPredicate:predicate] retain];
	return YES;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
	return [visibleSongs count];
}

- (NSDictionary *)_songAt:(NSIndexPath *)indexPath {
	if (indexPath == nil || [visibleSongs count] <= [indexPath row]) {
		return nil;
	}
	return [visibleSongs objectAtIndex:indexPath.row];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *CellIdentifier = @"SongCell";
    
    SongCell *cell = (SongCell *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[[SongCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier] autorelease];
    }

	NSDictionary *item = [self _songAt:indexPath];
	[cell assign:item];
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
	NSDictionary *song = [self _songAt:indexPath];
	selectedSong = song;
	[self songSelected:song atIndexPath:indexPath];
}

- (void)songPlayed:(NSDictionary *)song {
	[[FrontController sharedInstance] popAll];
	[[FrontController sharedInstance] showPlayingNow:song];
}

#define kAppend @"Append"
#define kPlayNext @"Play Next"
#define kPlayNow @"Play Now"
#define kNevermind @"Nevermind"

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
	NSString *title = [alertView buttonTitleAtIndex:buttonIndex];
	NSDictionary *song = selectedSong;
	if (song == nil) {
		return;
	}
	NSLog(@"Song: %@", song);
	
	if ([title isEqualToString:kAppend]) {
		[[MediaService sharedInstance] appendByKey:[song objectForKey:@"key"]];
	}
	if ([title isEqualToString:kPlayNext]) {
		[[MediaService sharedInstance] playByKeyNext:[song objectForKey:@"key"]];
	}
	if ([title isEqualToString:kPlayNow]) {
		[[MediaService sharedInstance] playByKeyNow:[song objectForKey:@"key"]];
		[self songPlayed:song];
	}
}

- (void)songSelected:(NSDictionary *)song atIndexPath:(NSIndexPath *)indexPath {
	NSLog(@"Song: %@ %@", song, indexPath);
	UIAlertView *alert = [[UIAlertView alloc]
						  initWithTitle:@"What do you want to do?"
						  message:nil
						  delegate:self
						  cancelButtonTitle:kNevermind
						  otherButtonTitles:nil];
	[alert addButtonWithTitle:kPlayNext];
	[alert addButtonWithTitle:kAppend];
	[alert addButtonWithTitle:kPlayNow];
    [alert show];
    [alert release];
}

- (void)playSong:(NSDictionary *)song atIndexPath:(NSIndexPath *)indexPath {
}

- (void)dealloc {
	[allSongs release];
	[visibleSongs release];
    [super dealloc];
}

@end

