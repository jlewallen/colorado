//
//  MediaService.h
//  Colorado
//
//  Created by Jacob Lewallen on 3/12/11.
//  Copyright 2011 None. All rights reserved.
//

#import <Foundation/Foundation.h>

@class NIRTimer;

@interface MediaService : NSObject {
	NSError *error;
	NIRTimer *timer;
}

@property (readonly) NSError *error;

+ (MediaService *)sharedInstance;
- (NSArray *)findPlayingNow;
- (NSArray *)findPlaylists;
- (NSArray *)findArtists;
- (NSArray *)findZones;
- (NSDictionary *)findCurrentlyPlaying;
- (NSArray *)findPlaylistSongs:(NSString*)playlistId;

- (NSURL *)relativeUrlOf:(NSString *)relativePath;

- (void)playByIndex:(int)index;
- (void)playByKeyNow:(NSString *)key;
- (void)playByKeyNext:(NSString *)key;
- (void)appendByKey:(NSString *)key;

- (void)playPause;
- (void)gotoNext;
- (void)gotoPrevious;
- (void)volume:(float)level;
- (void)changeZone:(NSString *)id;

- (NSArray *)search:(NSString *)term;

@end
