//
//  SongListController.h
//  Colorado
//
//  Created by Jacob Lewallen on 3/12/11.
//  Copyright 2011 None. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SongListController : UITableViewController {
	NSArray *visibleSongs;
	NSArray *allSongs;
	NSDictionary *selectedSong;
	NSPredicate *filter;
}

@property (nonatomic, retain) NSPredicate *filter;

- (void)needSongs;
- (void)showSongs:(NSArray *)s predicate:(NSPredicate *)predicate;
- (void)songSelected:(NSDictionary *)song atIndexPath:(NSIndexPath *)indexPath;
- (void)playSong:(NSDictionary *)song atIndexPath:(NSIndexPath *)indexPath;
- (void)songPlayed:(NSDictionary *)song;

@end
