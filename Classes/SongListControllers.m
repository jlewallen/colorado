//
//  PlayingNowController.m
//  Colorado
//
//  Created by Jacob Lewallen on 3/12/11.
//  Copyright 2011 None. All rights reserved.
//

#import "SongListControllers.h"
#import "ProgressViewController.h"
#import "MediaService.h"

@implementation CurrentPlaylistController

- (void)needSongs {
	[[ProgressViewController sharedInstance] show];

	dispatch_queue_t queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0ul);
	dispatch_async(queue, ^{
		NSArray *items = [[MediaService sharedInstance] findPlayingNow];
		[self showSongs:items predicate:nil];
    });	
}

- (void)viewDidLoad {
	[super viewDidLoad];
	[self setTitle:@"Current Playlist"];
}

- (void)songSelected:(NSDictionary *)song atIndexPath:(NSIndexPath *)indexPath {
	[[MediaService sharedInstance] playByIndex:[[song objectForKey:@"index"] intValue]];
	[self songPlayed:song];
}

@end

@implementation PlaylistController

@synthesize playlistId;

- (void)needSongs {
	[[ProgressViewController sharedInstance] show];

	dispatch_queue_t queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0ul);
	dispatch_async(queue, ^{
		NSArray *items = [[MediaService sharedInstance] findPlaylistSongs:playlistId];
		[self showSongs:items predicate:nil];
	});
}

@end
