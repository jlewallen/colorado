//
//  FrontController.m
//  Colorado
//
//  Created by Jacob Lewallen on 3/12/11.
//  Copyright 2011 None. All rights reserved.
//

#import "ColoradoAppDelegate.h"
#import "FrontController.h"
#import "SongListControllers.h"
#import "PlaylistsController.h"
#import "PlayingNowController.h"
#import "SearchController.h"
#import "ZonesController.h"
#import "ServersController.h"
#import "WaitingForServersController.h"
#import "MenuController.h"
#import "ArtistListController.h"
#import "MediaCenter.h"
#import "Common.h"

@implementation FrontController

@synthesize playingNowController;
@synthesize currentPlaylistController;
@synthesize playlistsController;
@synthesize navigationController;
@synthesize searchController;
@synthesize zonesController;
@synthesize serversController;
@synthesize waitingForServersController;
@synthesize menuController;
@synthesize window;
@synthesize artistListController;
@synthesize mediaCenter;

+ (FrontController *)sharedInstance {
	return [[ColoradoAppDelegate app] frontController];
}

- (FrontController *)initWithWindow:(UIWindow *)w {
	if (self = [super init]) {
		window = w;
		navigationController = [[UINavigationController alloc] init];
		mediaCenter = [[MediaCenter alloc] initWithDelegate:self];
		[window addSubview:navigationController.view];
		[self showWaitingForServers];
	}
	return self;
}

- (void)serversChanged:(NSArray *)servers activeServer:(MediaServer *)server {
	if ([servers count]) {
		NSLog(@"Showing Menu...");
		[self showMenu];
	}
	else {
		NSLog(@"Showing Waiting...");
		[self showWaitingForServers];
	}
}

- (void)activeServerChanged:(MediaServer *)server {
	NSLog(@"FC: Active server changed:%@", server);
	RELEASE_AND_NIL(currentPlaylistController);
 	RELEASE_AND_NIL(playlistsController);
	RELEASE_AND_NIL(playingNowController);
	RELEASE_AND_NIL(searchController);
	RELEASE_AND_NIL(zonesController);
	RELEASE_AND_NIL(serversController);
}

- (void)showWaitingForServers {
	if (waitingForServersController == nil) {
		waitingForServersController = [[WaitingForServersController alloc] initWithNibName:@"WaitingForServersController" bundle:[NSBundle mainBundle]];
	}
	// BOOL animated = menuController != nil;
	// [[self navigationController] presentModalViewController:waitingForServersController animated:animated];
	[navigationController.view addSubview:waitingForServersController.view];
}

- (void)showMenu {
	if (menuController == nil) {
		MenuController *controller = [[MenuController alloc] initWithNibName:@"MenuController" bundle:[NSBundle mainBundle]];
		menuController = controller;
		[self.navigationController pushViewController:menuController animated:NO];
	}
	[self.navigationController popToRootViewControllerAnimated:NO];
	[waitingForServersController.view removeFromSuperview];
	// [self.navigationController dismissModalViewControllerAnimated:YES];
}

- (void)showCurrentPlaylist {
	if (currentPlaylistController == nil) {
		CurrentPlaylistController *controller = [[CurrentPlaylistController alloc] initWithNibName:@"SongListController" bundle:[NSBundle mainBundle]];
		currentPlaylistController = controller;
	}
	[self.navigationController pushViewController:currentPlaylistController animated:YES];
}

- (void)showPlaylists {
	if (playlistsController == nil) {
		PlaylistsController *controller = [[PlaylistsController alloc] initWithNibName:@"PlaylistsController" bundle:[NSBundle mainBundle]];
		playlistsController = controller;
	}
	[self.navigationController pushViewController:playlistsController animated:YES];
}

- (void)showArtistListController {
	if (artistListController == nil) {
		ArtistListController *controller = [[ArtistListController alloc] initWithNibName:@"ArtistListController" bundle:[NSBundle mainBundle]];
		artistListController = controller;
	}
	[self.navigationController pushViewController:artistListController animated:YES];
}

- (void)showPlayingNow:(NSDictionary *)song {
	if (playingNowController == nil) {
		PlayingNowController *controller = [[PlayingNowController alloc] initWithNibName:@"PlayingNowController" bundle:[NSBundle mainBundle]];
		playingNowController = controller;
	}
	if (song == nil) {
		[playingNowController refresh];
	}
	else {
		[playingNowController show:song];
	}
	[self.navigationController pushViewController:playingNowController animated:YES];
}

- (void)createSearchController {
	if (searchController == nil) {
		SearchController *controller = [[SearchController alloc] initWithNibName:@"SearchController" bundle:[NSBundle mainBundle]];
		searchController = controller;
	}
}

- (void)showSearch {
	[self createSearchController];
	[self.navigationController pushViewController:searchController animated:YES];
}

- (void)showSearchFor:(NSString *)searchString {
	[self createSearchController];
	[[self searchController] searchFor:searchString predicate:nil block:YES];
	[self.navigationController pushViewController:searchController animated:YES];
}

- (void)showArtistSearchFor:(NSString *)searchString {
	[self createSearchController];
	[searchController searchArtist:searchString block:YES];
	[self.navigationController pushViewController:searchController animated:YES];
}

- (void)showZones {
	if (zonesController == nil) {
		ZonesController *controller = [[ZonesController alloc] initWithNibName:@"ZonesController" bundle:[NSBundle mainBundle]];
		zonesController = controller;
	}
	[self.navigationController pushViewController:zonesController animated:YES];
}

- (void)showServers {
	if (serversController == nil) {
		ServersController *controller = [[ServersController alloc] initWithNibName:@"ServersController" bundle:[NSBundle mainBundle]];
		serversController = controller;
	}
	[self.navigationController pushViewController:serversController animated:YES];
}

- (void)popAll {
	[self.navigationController popToRootViewControllerAnimated:FALSE];
}

- (void)dealloc {
	RELEASE_AND_NIL(currentPlaylistController);
 	RELEASE_AND_NIL(playlistsController);
	RELEASE_AND_NIL(playingNowController);
	RELEASE_AND_NIL(navigationController);
	RELEASE_AND_NIL(searchController);
	RELEASE_AND_NIL(zonesController);
	RELEASE_AND_NIL(serversController);
	RELEASE_AND_NIL(waitingForServersController);
	RELEASE_AND_NIL(artistListController);
	RELEASE_AND_NIL(window);
	RELEASE_AND_NIL(mediaCenter);
    [super dealloc];
}

@end
