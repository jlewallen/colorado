//
//  PlayingNowController.m
//  Colorado
//
//  Created by Jacob Lewallen on 3/12/11.
//  Copyright 2011 None. All rights reserved.
//

#include <math.h>

#import "Common.h"
#import "PlayingNowController.h"
#import "FrontController.h"
#import "MediaCenter.h"
#import "MediaService.h"

@implementation PlayingNowController

@synthesize visibleSong;
@synthesize nextButton;
@synthesize previousButton;
@synthesize playButton;
@synthesize pauseButton;
@synthesize artworkView;
@synthesize volumeSlider;
@synthesize nameLabel;
@synthesize artistLabel;
@synthesize albumLabel;

- (CGRect)_getRectOf:(CGSize)size in:(CGSize)frame {
	float hfactor = size.width / frame.width;
	float vfactor = size.height / frame.height;
	float factor = MAX(hfactor, vfactor);
	float newWidth = size.width / factor;
	float newHeight = size.height / factor;
	float leftOffset = (frame.width - newWidth) / 2;
	float topOffset = 0; // (frame.height - newHeight) / 2;
	return CGRectMake(leftOffset, topOffset, newWidth, newHeight);
}

- (UIImage *)_findArtwork:(NSDictionary *)info {
	NSString *relativeUrl = [info objectForKey:@"imageUrl"];
	NSURL *imageUrl = [[MediaService sharedInstance] relativeUrlOf:relativeUrl];
	if (imageUrl == nil) {
		return nil;
	}
	
	UIImage *image = [UIImage imageWithData:[NSData dataWithContentsOfURL:imageUrl]];
	if (image == nil || !CGSizeIsEitherAxisEmpty(image.size)) {
	}
	return image;
}

- (void)show:(NSDictionary *)info {
	NSLog(@"Showing: %@", info);
	dispatch_queue_t queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_LOW, 0ul);
	
	if (![[self.visibleSong objectForKey:@"key"] isEqualToString:[info objectForKey:@"key"]]) {
		self.visibleSong = info;

		dispatch_async(queue, ^{
			dispatch_sync(dispatch_get_main_queue(), ^{
				[artworkView setImage:nil];
				[nameLabel setText:[info objectForKey:@"name"]];
				[artistLabel setText:[info objectForKey:@"artist"]];
				[albumLabel setText:[info objectForKey:@"album"]];
			});
		});
		
		dispatch_async(queue, ^{
			UIImage *image = [self _findArtwork:info];
			
			dispatch_sync(dispatch_get_main_queue(), ^{
				if (image != nil && !CGSizeIsEitherAxisEmpty(image.size)) {
					CGRect fitted = [self _getRectOf:image.size in:artworkView.superview.frame.size];
					NSLog(@"Resizing %@ into %@ (got %@)", NSStringFromCGSize(image.size), NSStringFromCGRect(artworkView.frame), NSStringFromCGRect(fitted));
					[artworkView setContentMode:UIViewContentModeScaleAspectFit];
					[artworkView setFrame:fitted];
					[artworkView setImage:image];
				}
				else {
					[artworkView setImage:nil];
				}
			});
		});
	}

	dispatch_async(queue, ^{
		dispatch_sync(dispatch_get_main_queue(), ^{
			NSString *status = [info objectForKey:@"status"];
			if (status == nil || [status isEqualToString:@"Playing"]) {
				[playButton setHidden:YES];
				[pauseButton setHidden:NO];
			}
			else {
				[playButton setHidden:NO];
				[pauseButton setHidden:YES];
			}

			[volumeSlider setValue:[[info objectForKey:@"volume"] floatValue]];
		});
	});
}

- (void)refresh {
	dispatch_queue_t queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0ul);
	dispatch_async(queue, ^{
		NSDictionary *info = [[MediaCenter sharedInstance] findCurrentlyPlaying];
		if (info != nil) {
			NSLog(@"Refreshed: %@", info);
		}
		else {
			NSLog(@"Refreshed (Nothing)");
		}
		[self show:info];
	});	
}

- (UILabel *)_newNavigationLabel:(CGRect)frame {
	UILabel *label = [[UILabel alloc] initWithFrame:frame];
	label.backgroundColor = [UIColor clearColor];
	label.textColor = [UIColor whiteColor];
	label.adjustsFontSizeToFitWidth = YES;
	label.font = [UIFont boldSystemFontOfSize:12];
	label.textAlignment = UITextAlignmentCenter;
	return label;
}

- (void)_customizeNavigationBar {
	UIView *labels = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 240, 44)];
	artistLabel = [self _newNavigationLabel:CGRectMake(0, 1, 170, 13)];
	artistLabel.textColor = RGBA(0.6, 0.6, 0.6, 1);
	nameLabel = [self _newNavigationLabel:CGRectMake(0, 14, 170, 13)];
	albumLabel = [self _newNavigationLabel:CGRectMake(0, 27, 170, 13)];
	albumLabel.textColor = RGBA(0.6, 0.6, 0.6, 1);
	
	UIButton *button = [UIButton buttonWithType:UIButtonTypeDetailDisclosure];
	[button addTarget:[FrontController sharedInstance] action:@selector(showCurrentPlaylist) forControlEvents:UIControlEventTouchUpInside];
	[button setFrame:CGRectMake(198, 0, 50, 44)];
	[labels addSubview:button];
	
	// [labels setBackgroundColor:[UIColor orangeColor]];
	// [artistLabel setBackgroundColor:[UIColor greenColor]];
	
	[labels addSubview:artistLabel];
	[labels addSubview:nameLabel];
	[labels addSubview:albumLabel];
	
    UIBarButtonItem *item = [[UIBarButtonItem alloc] initWithCustomView:labels];
    self.navigationItem.rightBarButtonItem = item;
    [item release];
}

- (void)viewDidLoad {
	[super viewDidLoad];
	[self _customizeNavigationBar];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
	[self.navigationController.navigationBar setTintColor:[UIColor blackColor]];
	[[MediaCenter sharedInstance] addDelegate:self];
}

- (void)viewWillDisappear:(BOOL)animated {
	[[MediaCenter sharedInstance] removeDelegate:self];
    [super viewWillDisappear:animated];	
	[self.navigationController.navigationBar setTintColor:nil];
}

- (IBAction)togglePlaying:(id)sender {
	[[MediaService sharedInstance] playPause];
	[self refresh];
}

- (IBAction)gotoPrevious:(id)sender {
	[[MediaService sharedInstance] gotoPrevious];
	[self refresh];
}

- (IBAction)gotoNext:(id)sender {
	[[MediaService sharedInstance] gotoNext];
	[self refresh];
}

- (IBAction)changedVolume:(UISlider *)sender {
	[[MediaService sharedInstance] volume:[sender value]];
	[self refresh];
}

- (void)mediaCenter:(MediaCenter *)mc currentPlaying:(NSDictionary *)cp {
	dispatch_queue_t queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0ul);
	dispatch_async(queue, ^{
		[self show:cp];
	});
}

- (void)dealloc {
	[playButton release];
	[pauseButton release];
	[nextButton release];
	[previousButton release];
	[artworkView release];
	[volumeSlider release];
	[artistLabel release];
	[nameLabel release];
	[albumLabel release];
	[[MediaCenter sharedInstance] removeDelegate:self];
    [super dealloc];
}

@end

