//
//  PlaylistsController.h
//  Colorado
//
//  Created by Jacob Lewallen on 3/12/11.
//  Copyright 2011 None. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PlaylistsController : UITableViewController {
	NSArray *playlists;
	NSArray *groupings;
}

@end
