//
//  MenuController.m
//  Colorado
//
//  Created by Jacob Lewallen on 3/12/11.
//  Copyright 2011 None. All rights reserved.
//

#import "ColoradoAppDelegate.h"
#import "MenuController.h"
#import "FrontController.h"

@interface MenuItem : NSObject {
	NSString *title;
	SEL selected;
}

@property (nonatomic, retain) NSString *title;
@property (nonatomic) SEL selected;

- (id)initWithTitle:(NSString *)t selector:(SEL)s;

@end

@implementation MenuItem

@synthesize title;
@synthesize selected;

- (id)initWithTitle:(NSString *)t selector:(SEL)s {
	if (self = [super init]) {
		title = [t copy];
		selected = s;
	}
	return self;
}

- (void)dealloc {
	[title release];
	[super dealloc];
}

@end


@implementation MenuController

- (void)viewDidLoad {
	[super viewDidLoad];
	[self setTitle:@"JRMC"];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
	return [[self findMenuItems] count];
}

- (void)_showCurrentPlaylist {
	[[FrontController sharedInstance] showCurrentPlaylist];
}

- (void)_showPlaylists {
	[[FrontController sharedInstance] showPlaylists];
}

- (void)_showPlayingNow {
	[[FrontController sharedInstance] showPlayingNow:nil];
}

- (void)_showSearch {
	[[FrontController sharedInstance] showSearch];
}

- (void)_showZones {
	[[FrontController sharedInstance] showZones];
}

- (void)_showServers {
	[[FrontController sharedInstance] showServers];
}

- (void)_showArtistList {
	[[FrontController sharedInstance] showArtistListController];
}

- (NSArray *)findMenuItems {
	if (menuItems == nil) {
		menuItems = [[NSArray alloc] initWithObjects:
					 [[MenuItem alloc] initWithTitle:@"Playing Now" selector:@selector(_showPlayingNow)],
					 [[MenuItem alloc] initWithTitle:@"Playlists" selector:@selector(_showPlaylists)],
					 [[MenuItem alloc] initWithTitle:@"Search" selector:@selector(_showSearch)],
					 [[MenuItem alloc] initWithTitle:@"Artists" selector:@selector(_showArtistList)],
					 [[MenuItem alloc] initWithTitle:@"Zones" selector:@selector(_showZones)],
					 [[MenuItem alloc] initWithTitle:@"Servers" selector:@selector(_showServers)],
					 nil];
	}
	return menuItems;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *CellIdentifier = @"ManualCell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[[UITableViewCell alloc] initWithFrame:CGRectZero reuseIdentifier:CellIdentifier] autorelease];
    }
	NSArray *items = [self findMenuItems];
	MenuItem *item = [items objectAtIndex:indexPath.row];
	cell.textLabel.text = item.title;
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
	NSArray *items = [self findMenuItems];
	MenuItem *item = [items objectAtIndex:indexPath.row];
	[self performSelector:item.selected];
}

- (void)dealloc {
    [super dealloc];
}


@end

