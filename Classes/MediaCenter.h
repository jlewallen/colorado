//
//  MediaCenter.h
//  Colorado
//
//  Created by Jacob Lewallen on 3/13/11.
//  Copyright 2011 None. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "Broadcaster.h"

@class MediaCenter;
@class MediaServer;

@protocol MediaCenterDelegate <NSObject>
@optional
- (void)serversChanged:(NSArray *)servers activeServer:(MediaServer *)server;
- (void)activeServerChanged:(MediaServer *)server;
- (void)mediaCenter:(MediaCenter *)mc currentPlaying:(NSDictionary *)cp;
@end

@interface MediaCenter : NSObject <AutomaticDiscoveryDelegate> {
	NSTimer *timer;
	NSMutableSet *delegates;
	NSMutableDictionary *servers;
	Broadcaster *broadcaster;
	MediaServer *activeServer;
	NSDate *lastPinged;
}

@property (nonatomic, retain) NSDate *lastPinged;
@property (nonatomic, retain) NSDictionary *servers;
@property (nonatomic, retain) MediaServer *activeServer;

+ (MediaCenter *)sharedInstance;
- (MediaCenter *)initWithDelegate:(id <MediaCenterDelegate>)delegate;
- (NSDictionary *)findCurrentlyPlaying;
- (void)addDelegate:(id <MediaCenterDelegate>)delegate;
- (void)removeDelegate:(id <MediaCenterDelegate>)delegate;
- (void)use:(MediaServer *)server;

@end
