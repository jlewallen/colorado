//
//  Common.h
//  Colorado
//
//  Created by Jacob Lewallen on 3/13/11.
//  Copyright 2011 None. All rights reserved.
//

#import <Foundation/Foundation.h>

#define RGBA(r, g, b, a) [UIColor colorWithRed:r green:g blue:b alpha:a]
#define CGSizeIsEmpty(s) (s.width == 0 && s.height == 0)
#define CGSizeIsEitherAxisEmpty(s) (s.width == 0 || s.height == 0)

#define RELEASE_AND_NIL(v) if ((v) != nil) { [v release]; v = nil; }

