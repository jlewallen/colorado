//
//  SearchController.m
//  Colorado
//
//  Created by Jacob Lewallen on 3/13/11.
//  Copyright 2011 None. All rights reserved.
//

#import "SearchController.h"
#import "FrontController.h"
#import "MediaService.h"
#import "ProgressViewController.h"

@implementation SearchController

- (void)viewDidLoad {
	[super viewDidLoad];
	[self setTitle:@"Search"];
}

- (NSString *)currentTerm {
    @synchronized(self) {
        return [[term copy] autorelease];
    }
}

- (void)setCurrentTerm:(NSString *)t {
    @synchronized(self) {
        if (term != t) {
            [term release];
            term = [t copy];
        }
    }
}

- (void)searchArtist:(NSString *)searchString block:(BOOL)block {
	NSPredicate *predicate = [NSPredicate predicateWithFormat:@"SELF.artist = %@", searchString];
	[self searchFor:searchString predicate:predicate block:block];
}

- (void)searchFor:(NSString *)searchString predicate:(NSPredicate *)predicate block:(BOOL)block {
	NSLog(@"Search: %@", searchString);
	[self setCurrentTerm:searchString];
	
	if (block) {
		[[ProgressViewController sharedInstance] show];
	}
	if ([searchString length] >= 3) {
		dispatch_queue_t queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0ul);
		dispatch_async(queue, ^{
			NSArray *items = [[[MediaService sharedInstance] search:searchString] retain];
			[self showSongs:items predicate:predicate];
			[items release];
			
			dispatch_sync(dispatch_get_main_queue(), ^{
				if ([searchString isEqualToString:self.currentTerm]) {
					[self.searchDisplayController.searchResultsTableView reloadData];
					if (block) {
						[[ProgressViewController sharedInstance] hide];
					}
				}
			});
		});
	}
}

- (void)searchDisplayControllerDidBeginSearch:(UISearchDisplayController *)controller {
}

- (void)searchDisplayControllerDidEndSearch:(UISearchDisplayController *)controller {
}

- (BOOL)searchDisplayController:(UISearchDisplayController *)controller shouldReloadTableForSearchScope:(NSInteger)searchOption {
	return YES;
}

- (BOOL)searchDisplayController:(UISearchDisplayController *)controller shouldReloadTableForSearchString:(NSString *)searchString {
	[self searchFor:searchString predicate:nil block:NO];
	return NO;
}

- (void)release {
	[super release];
}

- (void)dealloc {
	NSLog(@"SearchController: Dealloc");
    [super dealloc];
}

@end
