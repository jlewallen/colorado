//
//  SongCell.m
//  Colorado
//
//  Created by Jacob Lewallen on 3/12/11.
//  Copyright 2011 None. All rights reserved.
//

#import "TableViewCells.h"
#import "MediaServer.h"

@implementation MultiLineTableViewCell

@synthesize topLabel;
@synthesize bottomLabel;

- (UILabel *)newLabelWithPrimaryColor:(UIColor *)primaryColor selectedColor:(UIColor *)selectedColor fontSize:(CGFloat)fontSize bold:(BOOL)bold {
    UIFont *font;
    if (bold) {
        font = [UIFont boldSystemFontOfSize:fontSize];
    }
	else {
        font = [UIFont systemFontOfSize:fontSize];
    }
	UILabel *label = [[UILabel alloc] initWithFrame:CGRectZero];
	label.backgroundColor = [UIColor whiteColor];
	label.opaque = YES;
	label.textColor = primaryColor;
	label.highlightedTextColor = selectedColor;
	label.font = font;	
	return label;
}

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
	if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
		UIView *view = self.contentView;
		
		self.topLabel = [self newLabelWithPrimaryColor:[UIColor blackColor] selectedColor:[UIColor whiteColor] fontSize:14.0 bold:YES];
		self.topLabel.textAlignment = UITextAlignmentLeft;
		self.topLabel.adjustsFontSizeToFitWidth = YES;
		[view addSubview:self.topLabel];
		[self.topLabel release];
		
        self.bottomLabel = [self newLabelWithPrimaryColor:[UIColor blackColor] selectedColor:[UIColor lightGrayColor] fontSize:11.0 bold:NO];
		self.bottomLabel.textAlignment = UITextAlignmentLeft;
		self.bottomLabel.adjustsFontSizeToFitWidth = YES;
		[view addSubview:self.bottomLabel];
		[self.bottomLabel release];
	}
	
	return self;
}

- (void)layoutSubviews {
	[super layoutSubviews];
    if (!self.editing) {
        CGFloat x =  self.contentView.bounds.origin.x;
		self.topLabel.frame = CGRectMake(x + 10, 4, 200, 20);
		self.bottomLabel.frame = CGRectMake(x + 10, 28, 200, 14);
	}
}

- (void)dealloc {
	[topLabel release];
	[bottomLabel release];
	[super dealloc];
}

@end

@implementation SongCell

- (void)assign:(NSDictionary *)song {
	[topLabel setText:[song objectForKey:@"name"]];
	[bottomLabel setText:[song objectForKey:@"artist"]];
}

@end

@implementation ServerCell

- (void)assign:(MediaServer *)server {
	if ([server active]) {
		self.accessoryType = UITableViewCellAccessoryCheckmark;
	}
	else {
		self.accessoryType = UITableViewCellAccessoryNone;
	}	
	[topLabel setText:[server name]];
	[bottomLabel setText:[server address]];
}

@end