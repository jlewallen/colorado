//
//  PlayingNowController.h
//  Colorado
//
//  Created by Jacob Lewallen on 3/12/11.
//  Copyright 2011 None. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SongListController.h"

@interface CurrentPlaylistController : SongListController {
}

@end

@interface PlaylistController : SongListController {
	NSString *playlistId;
}

@property (nonatomic, retain) NSString *playlistId;

@end
