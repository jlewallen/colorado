//
//  WaitingForServersController.h
//  Colorado
//
//  Created by Jacob Lewallen on 3/14/11.
//  Copyright 2011 None. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface WaitingForServersController : UIViewController {
	IBOutlet UIActivityIndicatorView *activityView;
}

@property (nonatomic, retain) IBOutlet UIActivityIndicatorView *activityView;

@end
