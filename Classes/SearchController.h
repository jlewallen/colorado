//
//  SearchController.h
//  Colorado
//
//  Created by Jacob Lewallen on 3/13/11.
//  Copyright 2011 None. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "SongListController.h"

@interface SearchController : SongListController <UISearchDisplayDelegate> {
	NSString *term;
}

- (void)searchFor:(NSString *)searchString predicate:(NSPredicate *)predicate block:(BOOL)b;
- (void)searchArtist:(NSString *)searchString block:(BOOL)b;

@end
