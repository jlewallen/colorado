//
//  ArtistListController.h
//  Colorado
//
//  Created by Jacob Lewallen on 4/26/11.
//  Copyright 2011 None. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ArtistListController : UITableViewController {
	NSArray *groups;
	NSArray *artists;
	NSArray *indices;
}

@end
