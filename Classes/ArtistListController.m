//
//  ArtistListController.m
//  Colorado
//
//  Created by Jacob Lewallen on 4/26/11.
//  Copyright 2011 None. All rights reserved.
//

#import "ArtistListController.h"
#import "MediaService.h"
#import "ProgressViewController.h"
#import "FrontController.h"

@interface ArtistGrouping : NSObject {
	NSString *title;
	NSArray *items;
}

@property (nonatomic, retain) NSString *title;
@property (nonatomic, retain) NSArray *items;

- (ArtistGrouping *)initWithTitle:(NSString *)t items:(NSArray *)i;

@end

@implementation ArtistGrouping

@synthesize title;
@synthesize items;

- (ArtistGrouping *)initWithTitle:(NSString *)t items:(NSArray *)i {
	if (self = [super init]) {
		self.title = t;
		self.items = i;
	}
	return self;
}

- (void)dealloc {
	[items release];
	[super dealloc];
}

@end

@implementation ArtistListController

- (void)viewDidLoad {
    [super viewDidLoad];
	[self setTitle:@"Artists"];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
	
	if (artists == nil) {
		artists = [[NSArray alloc] init];
	}
	if (groups == nil) {
		groups = [[NSArray alloc] init];
	}
	if (indices == nil) {
		indices = [[NSArray alloc] init];
	}
	
	[[ProgressViewController sharedInstance] show];
	
	dispatch_queue_t queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0ul);
	dispatch_async(queue, ^{
		NSArray *items = [[MediaService sharedInstance] findArtists];
		[indices release];
		[groups release];
		[artists release];
		artists = [items retain];

		NSString *letters = @"ABCDEFGHIJKLMNOPQRSTUVWXYZ";
		NSString *keys = [@"#" stringByAppendingString:letters];
		NSMutableDictionary *boxes = [[[NSMutableDictionary alloc] init] autorelease];
		for (NSDictionary *row in artists) {
			NSString *name = [row objectForKey:@"name"];
			NSString *title = [[name substringToIndex:1] uppercaseString];
			if (title == nil || [letters rangeOfString:title].location == NSNotFound) {
				title = @"#";
			}
			if ([boxes objectForKey:title] == nil) {
				[boxes setObject:[[NSMutableArray alloc] init] forKey:title];
			}
			[[boxes objectForKey:title] addObject:row];
		}

		NSMutableArray *t = [[[NSMutableArray alloc] init] autorelease];
		NSMutableArray *g = [[[NSMutableArray alloc] init] autorelease];
		for (int i = 0; i < [keys length]; ++i) {
			NSString *key = [keys substringWithRange:NSMakeRange(i, 1)];
			NSArray *items = [boxes objectForKey:key];
			[g addObject:[[ArtistGrouping alloc] initWithTitle:key items:items]];
			[t addObject:key];
		}
		
		groups = [[NSArray alloc] initWithArray:g];
		indices = [[NSArray alloc] initWithArray:t];
		
        dispatch_sync(dispatch_get_main_queue(), ^{
			[self.tableView reloadData];
			[[ProgressViewController sharedInstance] hide];
        });
    });	
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
}

- (void)viewDidDisappear:(BOOL)animated {
    [super viewDidDisappear:animated];
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
	return [groups count];
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {
	return [[groups objectAtIndex:section] title];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
	return [[[groups objectAtIndex:section] items] count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *CellIdentifier = @"ArtistCell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier] autorelease];
    }
	
	ArtistGrouping *group = [groups objectAtIndex:indexPath.section];
	NSDictionary *item = [[group items] objectAtIndex:indexPath.row];
	cell.textLabel.text = [item objectForKey:@"name"];
    return cell;
}

- (NSArray *)sectionIndexTitlesForTableView:(UITableView *)tableView {
    return indices;
}

- (NSInteger)tableView:(UITableView *)tableView sectionForSectionIndexTitle:(NSString *)title atIndex:(NSInteger)index {
    return [indices indexOfObject:title];
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
	ArtistGrouping *group = [groups objectAtIndex:indexPath.section];
	NSDictionary *item = [[group items] objectAtIndex:indexPath.row];
	NSLog(@"%@", item);
	[[FrontController sharedInstance] showArtistSearchFor:[item objectForKey:@"name"]];
}

- (void)viewDidUnload {
}

- (void)dealloc {
	[artists release];
    [super dealloc];
}

@end
