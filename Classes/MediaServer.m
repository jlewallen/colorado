//
//  MediaServer.m
//  Colorado
//
//  Created by Jacob Lewallen on 3/13/11.
//  Copyright 2011 None. All rights reserved.
//

#import "MediaServer.h"

@implementation MediaServer

@synthesize name;
@synthesize address;
@synthesize active;

- (MediaServer *)initWithName:(NSString *)n address:(NSString *)a {
	if (self = [super init]) {
		name = [n copy];
		address = [a copy];
		active = NO;
		[self touch];
	}
	return self;
}

- (NSString *)description {
	return address;
}

- (void)touch {
	@synchronized(self) {
		[lastSeen release];
		lastSeen = [[NSDate alloc] init];
	}
}

- (NSInteger)lastSeenIn {
	@synchronized(self) {
		return [[NSDate date] timeIntervalSinceDate:lastSeen];
	}
}

- (BOOL)seenIn:(int)seconds {
	return [self lastSeenIn] < seconds;
}

- (void)dealloc {
	[name release];
	[address release];
	[lastSeen release];
	[super dealloc];
}

@end