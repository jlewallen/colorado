//
//  PlayingNowController.h
//  Colorado
//
//  Created by Jacob Lewallen on 3/12/11.
//  Copyright 2011 None. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "MediaCenter.h"

@interface PlayingNowController : UIViewController <MediaCenterDelegate> {
	NSDictionary *visibleSong;
	IBOutlet UIImageView *artworkView;
	IBOutlet UISlider *volumeSlider;
	IBOutlet UILabel *artistLabel;
	IBOutlet UILabel *nameLabel;
	IBOutlet UILabel *albumLabel;
	IBOutlet UIButton *playButton;
	IBOutlet UIButton *pauseButton;
	IBOutlet UIButton *nextButton;
	IBOutlet UIButton *previousButton;
}

@property (nonatomic, retain) NSDictionary *visibleSong;
@property (nonatomic, retain) IBOutlet UIButton *playButton;
@property (nonatomic, retain) IBOutlet UIButton *pauseButton;
@property (nonatomic, retain) IBOutlet UIButton *nextButton;
@property (nonatomic, retain) IBOutlet UIButton *previousButton;
@property (nonatomic, retain) IBOutlet UIImageView *artworkView;
@property (nonatomic, retain) IBOutlet UISlider *volumeSlider;
@property (nonatomic, retain) IBOutlet UILabel *artistLabel;
@property (nonatomic, retain) IBOutlet UILabel *nameLabel;
@property (nonatomic, retain) IBOutlet UILabel *albumLabel;

- (void)refresh;
- (void)show:(NSDictionary *)song;
- (IBAction)togglePlaying:(id)sender;
- (IBAction)gotoPrevious:(id)sender;
- (IBAction)gotoNext:(id)sender;
- (IBAction)changedVolume:(UISlider *)sender;

@end
